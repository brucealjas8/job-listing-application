<?php

namespace App\Http\Controllers;

use App\Models\Listing;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Termwind\Components\Li;

class ListingController extends Controller
{
    //Show all listings
    public function index() {
//        $listing = Listing::first();
//        dd(explode(',', $listing->tags));
//        dd($listing);
        return view('listings', [
            'listings' => Listing::latest()->filter(request(['tag', 'search']))->get()
        ]);
    }

    public function show(Listing $listing) {
        return view('listing', [
            'listing' => $listing
        ]);
    }
    public function create() {
        return view('create');
    }
    public function store(Request $request) {
        $this->validate($request, [
            'company' => ['required', Rule::unique('listings','company')],
            'title' => 'required',
            'location' => 'required',
            'email' => 'required',
            'website' => 'required',
            'tags' => 'required',
            'description' => 'required',
        ]);


        Session::flash('message', 'Listing Created');

        $listing = new Listing();
        //On left field name in DB and on right field name in Form/view/request
        $listing->company = $request->company;
        $listing->title = $request->title;
        $listing->location = $request->location;
        $listing->email = $request->email;
        $listing->website = $request->website;
        $listing->tags = $request->tags;
        $listing->description = $request->description;
        $listing->save();

        return redirect('/')->with('message', 'Listing created successfully!');
    }
}
