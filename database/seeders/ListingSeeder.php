<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ListingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('listings')->insert([
            'title' => 'Testing Company',
            'tags' => Str::random(10),
            'company' => Str::random(10),
            'location' => Str::random(10),
            'email' => Str::random(10).'@gmail.com',
            'website' => Str::random(10),
            'description' => Str::random(50),
        ]);
    }
}
